import vweb

import account

struct Server {
	vweb.Context
mut:
	accounts shared account.Accounts = account.Accounts{}
}

fn main() {
	mut s := &Server{}
	
	lock s.accounts {
		s.accounts.connect_db()!
	}

	vweb.run_at(s, port: 3000, family: .ip6)!
}