module account

import rand
import time

struct SessionToken {
	token string
	user string
	last_used i64
}

pub fn (mut a Accounts) login(username string, password string) !string {
	users := sql a.db {
		select from User where username == username && password == password
	}!

	if users.len == 0 {
		return error('No user found')
	}

	return a.create_token(users[0])!
}

pub fn (mut a Accounts) create_token(user User) !string {
	token := SessionToken {
		rand.uuid_v4()
		user.username
		time.now().unix_time()
	}

	sql a.db {
		insert token into SessionToken
	}!

	// a.set_cookie(name: 'token', value: token.token)

	return token.token
}

pub fn (mut a Accounts) delete_token(user_token string) {
	sql a.db {
		delete from SessionToken where token == user_token
	} or { eprintln('Failed to delete token: ${err.msg()}') }

	// a.set_cookie(name: 'token', value: '')
}

pub fn (mut a Accounts) check_token(user_token string) bool {
	// user_token := a.get_cookie('token') or { return false }
	if user_token == '' {
		return false
	}

	matching_tokens := sql a.db {
		select count from SessionToken where token == user_token
	} or {
		return false
	}

	if matching_tokens > 0 {
		a.update_tokens(user_token)
		return true
	}
	return false
}

pub fn (mut a Accounts) user_from_token(user_token string) !User {	
	token_user := sql a.db {
		select from SessionToken where token == user_token
	}!

	if token_user.len == 0 {
		return error('No token found')
	}

	users := sql a.db {
		select from User where username == token_user[0].user
	}!

	a.update_tokens(user_token)

	if users.len == 0 {
		return error('No user found')
	}

	return users[0]
}

pub fn (mut a Accounts) update_tokens(current string) {
	now := time.now().unix_time()
	sql a.db {
		update SessionToken set last_used = now where token == current
	} or { eprintln('Failed to update token timestamp: ${err.msg()}') }

	// Clean all tokens more than one day old
	one_day_ago := time.now().unix_time() - 86400
	sql a.db {
		delete from SessionToken where last_used < one_day_ago
	} or { eprintln('Failed to clean tokens: ${err.msg()}') }
}