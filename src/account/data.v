module account

import db.sqlite

const database_file = 'data.db'

pub fn (mut a Accounts) connect_db() ! {
	a.db = sqlite.connect(database_file)!

	sql a.db {
		create table User
		create table SessionToken
		create table InviteCode
	}!
}

// pub fn (mut a Accounts) one() {
// 	a.connected = true
// }

// pub fn (mut a Accounts) two() {
// 	println(a.connected)
// }

pub fn (mut a Accounts) add_user(user User) ! {
	sql a.db {
		insert user into User
	}!
}

pub fn (mut a Accounts) change_password() ! {
	return error('not implemented')
}

pub fn (mut a Accounts) delete_user(user User) ! {
	sql a.db {
		delete from User where username == user.username
	}!
}