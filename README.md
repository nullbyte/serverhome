# My personal homepage

This repository contains the source code for my personal homepage (https://sirsegv.fryer.net.au), which shows the status of some of the services I run as well as an account system I made with the intention of providing a single sign-on point for all my hosted services (matrix, fediverse, game servers, etc.)